/**
 ** RaJS - Javascript Raytracer using HTML5 Workers
 **
 ** Author: Mattias Nilsson (mrorigo nospam @t gmail) 2010-08
 **
 ** Free to use and abuse
 **/

/* inlined vector math stuff */
ilength = function(b) {
    if(b.x == 0 && b.y == 0 && b.z == 0)
        return 1.0;
    return 1.0 / Math.sqrt( (b.x*b.x + b.y*b.y + b.z*b.z) );
};

normalize = function(b) {
    var d = ilength(b);
    if(d == 0)
        return b;
    return {x: b.x*d, y: b.y*d, z: b.z*d};
};
dotProduct = function(a,b) { return a.x*b.x + a.y*b.y + a.z*b.z; };
crossProduct = function(a, b) { return  {x: a.y*b.z - a.z*b.y, y: a.z*b.x - a.x*b.z, z: a.x*b.y - a.y*b.x}; };



var MAXTRACEDEPTH = 8;
var EPSILON = 0.0001;
var PRIM_POINTLIGHT = 1;
var PRIM_PLANE      = 2;
var PRIM_SPHERE     = 3;
var PRIM_TRIANGLE   = 4;

Triangle_getNormal = function(triangle, pos)
{
    if(triangle.normal)
        return triangle.normal;

    var a = triangle.vertices[0];
    var b = triangle.vertices[1];
    var c = triangle.vertices[2];
    var v1 = {x: b.x - a.x,
              y: b.y - a.y,
              z: b.z - a.z};
    var v2 = {x: c.x - a.x,
              y: c.y - a.y,
              z: c.z - a.z};
    triangle.normal = normalize(crossProduct(v1, v2));
    return triangle.normal;
}

Sphere_getNormal = function(sphere, pos)
{
    // Normalize by dividing by radius, don't normalize() (gives a sqrt)
    return {x: (pos.x - sphere.center.x)/sphere.radius,
            y: (pos.y - sphere.center.y)/sphere.radius,
            z: (pos.z - sphere.center.z)/sphere.radius};
}


var Engine = function() {

    workerInit = function(id, context) {
        this.workerId = id;
        this.workerContext = context;
    }

    findClosest = function(ray, currDist, Prim, nPrim) {
        var retval = 0;
        var hitPrim = false;
        for(var i=0; i<nPrim; i++) {
            var res=false;
            switch(Prim[i].type) {
            case PRIM_SPHERE:
                res = SphereIntersectRay(Prim[i], ray, currDist);
                break;
            case PRIM_TRIANGLE:
                res = TriangleIntersectRay(Prim[i], ray, currDist);
                break;
            case PRIM_PLANE:
                res = PlaneIntersectRay(Prim[i], ray, currDist);
                break;
            default:
                res = false;
                break;
            }

            if(res !== false) {
                //postMessage({type: 'message', message: "worker hit, dist="+res[1]});
                if(res[1] < currDist) {
                    retval = res[0];
                    currDist = res[1];
                    hitPrim = Prim[i];
                }
            }

        }

        return [retval, hitPrim, currDist];
    }

    traceRay = function(context, ray, currDepth, col, currRIndex) {
        if(currDepth > MAXTRACEDEPTH)
            return false;
        var currDist = 100001;
        var Prim = context.m_Scene.primitives;
        var nPrim = context.m_Scene.primitives.length;
        var cRes = findClosest(ray, currDist, Prim, nPrim);
        if(cRes == false)
            return false;
        var hitRes = cRes[0];
        var hitPrim = cRes[1];
        currDist = cRes[2];
        if(hitRes == 0) {
            col = {r:0,g:0,b:0};
        } else {
            if(hitPrim.type == PRIM_POINTLIGHT) {
                col = hitPrim.color;
                hitRes = 1;
            }
            else {
                var hitPoint = {x: ray.origin.x + ray.direction.x * currDist,
                                y: ray.origin.y + ray.direction.y * currDist,
                                z: ray.origin.z + ray.direction.z * currDist};
                var N=hitPrim.normal;
                if(!N)
                    switch(hitPrim.type) {
                    case PRIM_SPHERE:
                        N = Sphere_getNormal(hitPrim, hitPoint);
                        break;
                    case PRIM_TRIANGLE:
                        N = Triangle_getNormal(hitPrim, hitPoint);
                        break;
                    default:
                        N = {x:1, y:0, z:0};
                        break;
                    case PRIM_PLANE:
                        N = hitPrim.normal;
                        break;
                    }

                var pMat = context.m_Scene.materials[hitPrim.material];
                for(var i=0; i<nPrim; i++) {
                    if(Prim[i].type !== PRIM_POINTLIGHT)
                        continue;
                    var lPrim = Prim[i];

                    var L = normalize({x: lPrim.center.x - hitPoint.x,
                                       y: lPrim.center.y - hitPoint.y,
                                       z: lPrim.center.z - hitPoint.z});

                    // Diffuse light
                    var dot = dotProduct(L,N);
                    if(pMat.diffuse > 0 && dot > 0) { // We are lit
                        var diff = (pMat.diffuse * dot);
                        col.r += diff * pMat.color.r * lPrim.color.r;
                        col.g += diff * pMat.color.g * lPrim.color.g;
                        col.b += diff * pMat.color.b * lPrim.color.b;
                    }

                    // Specular light
                    if(pMat.specular > 0) {
                        var V = ray.direction;
                        var LdN2 = 2.0 * dot; //Product(L,N);
                        var R = {x: L.x - LdN2 * N.x,
                                 y: L.y - LdN2 * N.y,
                                 z: L.z - LdN2 * N.z};
                        var dot2 = dotProduct(V,R);
                        if(dot2 > 0) {
                            var spec = Math.pow(dot2, 10) * pMat.specular;
                            col.r += spec * lPrim.color.r;
                            col.g += spec * lPrim.color.g;
                            col.b += spec * lPrim.color.b;
                        }
                    }
                }

                if(pMat.reflection > 0) {
                    // Reflected dir = 2 * dot(dir, normal)
                    var rdN2 = 2.0 * dotProduct(ray.direction, N);

                    // Calculate reflected ray
                    var R = normalize({x: ray.direction.x - (N.x * rdN2),
                                       y: ray.direction.y - (N.y * rdN2),
                                       z: ray.direction.z - (N.z * rdN2)});
                    var rray = {origin: {x: hitPoint.x + R.x * EPSILON,
                                         y: hitPoint.y + R.y * EPSILON,
                                         z: hitPoint.z + R.z * EPSILON},
                                direction: R};
                    var col2 = {r:0, g:0, b:0};
                    // Send reflection ray
                    var rRet = traceRay(context, rray, currDepth+1, col2, currRIndex);
                    if(rRet !== false) {
                        var refl = pMat.reflection;
                        col2 = rRet[2];

                        col.r += col2.r * refl * pMat.color.r;
                        col.g += col2.g * refl * pMat.color.g;
                        col.b += col2.b * refl * pMat.color.b;
                    }
                    context.m_ReflectionRays++;
                }



            }
        }
        return [hitPrim, hitRes, col, currDist];
    }

    render = function(context) {
        if(context.m_CurrRow > context.m_MaxRow) {
            return false;
        }

        var SY = context.m_Scene.viewPlane.y1 + context.m_CurrRow * context.m_DY;
        var y = context.m_CurrRow;
        var poff = 0; // pixel offset
        var pixels = new Array(context.m_Scene.screen.width);
        var eye = context.m_Scene.eyePosition;

        for(var x = 0, SX = context.m_Scene.viewPlane.x1;
            x < context.m_Scene.screen.width;
            x++ )
        {
            var ray = {origin: eye,
                       direction: normalize({x: SX - eye.x,
                                             y: SY - eye.y,
                                             z: 0 - eye.z})};

            //    postMessage({type: 'message',
            //  message: 'ray.dir:'+ray.direction.x + ","+ray.direction.y+","+ray.direction.z});

            var col={r:0, g:0, b:0};
            var res = traceRay(context, ray, 1, col, 1);
            if(res != false)
                col = res[2];
                  /*      col.r = Math.min(col.r, 1.0);
      col.g = Math.min(col.g, 1.0);
      col.b = Math.min(col.b, 1.0);*/
            col.r *= 255;
            col.g *= 255;
            col.b *= 255;
            pixels[x*3+0] = col.r;
            pixels[x*3+1] = col.g;
            pixels[x*3+2] = col.b;

            SX += context.m_DX;
        }

            /*
    // Render 1 scanline and return it.
    var scanline = new Array(context.m_Scene.screen.width);
    for(var i=0; i<context.m_Scene.screen.width; i++) {
      scanline[i]=i;
    }
            */
        return pixels;
    }

    serverRun = function() {
        //    console.dir(this.workers);
        for(var i=0; i<this.workers.length; i++)
            this.workers[i].worker.postMessage({type: 'render'});
    }

    drawScan = function(y, scanline) {
        var pixels =  this.m_Screen.data;
        var offs =  y*(this.m_Canvas.width<<2);
        for(var x = 0, x2=0;
            x < this.m_Canvas.width;
            x++) {
            pixels[offs++] = scanline[x2++];
            pixels[offs++] = scanline[x2++];
            pixels[offs++] = scanline[x2++];
            pixels[offs++] = 255;
        }

        // Draw the image once per second max
        var now = (new Date()).getTime();
        if(!this.lastDrawScan || now - this.lastDrawScan > 1000) {
            this.m_Ctx.putImageData(this.m_Screen, 0, 0);
            this.lastDrawScan = now;
        }
    }

    serverInit = function(scene, canvas, numWorkers)
    {
        this.m_Canvas = canvas;
        this.m_Ctx = canvas.getContext('2d');
        //this.m_Screen = this.m_Ctx.createImageData(canvas.width, canvas.height);
        this.m_Screen = this.m_Ctx.getImageData(0,0, canvas.width, canvas.height);

        // Enumerate primitives and cull if facing away from camera (normal z < 0)..
        var ZA = {x:0,y:0,z:-1};
        for(var i=0; i < scene.primitives.length; i++) {
            scene.primitives[i]._id = i;
        }

        // Each worker get a part of the screen to render
        var syd = canvas.height / numWorkers;
        var currY=0;

        this.workers = [];
        for(var i=0; i<numWorkers; i++) {
            //      var w = new Worker("rt-worker"+(i+1)+".js");
            var w = new Worker("rt-worker.js");
            w.onerror = function(evt) {
                alert("An error occured in a worker: " + evt.filename + ":"+evt.lineno+":"+evt.message);
                //console.dir(evt);
            }

            var self = this;
            w.onmessage = function(evt) {
                var data = evt.data;
                switch(data.type) {
                case 'scanline':
                    self.drawScan(data.y, data.data);
                    break;

                case 'message':
                    //  console.log("Worker sent a message: " + data.message);
                    break;

                case 'finished':
                    for(var i=0; i<self.workers.length; i++) {
                        if(self.workers[i].id == data.worker_id) {
                            self.workers[i].worker.terminate();
                            self.workers.splice(i,1);
                            break;
                        }
                    }
                    self.m_Ctx.putImageData(self.m_Screen, 0, 0);
                    if(self.workers.length == 0)
                        self.onFinished();

                    //  console.log("Worker " + data.worker_id + " finished until " + data.y);
                    break;
                }
            }

            this.workers.push({worker: w, id: i});

            // Tell the worker who he is, and give him his context
            w.postMessage({type: 'id', id: i, context: makeContext(scene, "context " + i, currY, currY+syd)});

            currY += syd;
        }
        //    console.log("Server inited. Workers started.");
        return true;
    }


    makeContext = function(a_Scene, a_Name, a_startY, a_endY)
    {
        // View plane deltas
        var DX = (a_Scene.viewPlane.x2 - a_Scene.viewPlane.x1) / a_Scene.screen.width;
        var DY = (a_Scene.viewPlane.y2 - a_Scene.viewPlane.y1) / a_Scene.screen.height;
        return {m_Done: false,
                m_Scene: a_Scene,
                m_Name: a_Name,
                m_CurrRow: a_startY,
                m_DX: DX,
                m_DY: DY,
                // Current view Y
                m_lastPrim: false,  // Last primitive a ray hit
                m_MaxRow: a_endY,
                m_TimePassed: 0,
                m_PrimaryRays: 0,
                m_ReflectionRays: 0,
                m_RefractionRays: 0,
                m_ShadowRays: 0
               };
    }

    TriangleIntersectRay = function(triangle, a_Ray, a_Dist) {


        var v0 = triangle.vertices[0];
        var v1 = triangle.vertices[1];
        var v2 = triangle.vertices[2];
        if(!triangle.m_pE1)
            triangle.m_pE1 = {x: v1.x - v0.x,
                              y: v1.y - v0.y,
                              z: v1.z - v0.z};
        var m_pE1 = triangle.m_pE1;

        if(!triangle.m_pE2)
            triangle.m_pE2 = {x: v2.x - v0.x,
                              y: v2.y - v0.y,
                              z: v2.z - v0.z};
        var m_pE2 = triangle.m_pE2;

        var h = crossProduct(a_Ray.direction, m_pE2);
        var a = dotProduct(m_pE1, h);
        if(a > -EPSILON && a < EPSILON)
            return false;

        var f = 1.0/a;
        var s = {x: a_Ray.origin.x - v0.x,
                 y: a_Ray.origin.y - v0.y,
                 z: a_Ray.origin.z - v0.z};
        var u = f * dotProduct(s, h);
        if(u < 0.0 || u > 1.0)
            return false; // miss


        var q = crossProduct(s, m_pE1);
        var v = f * dotProduct(q, a_Ray.direction);
        if(v < 0.0 || u + v > 1.0)
            return false; // miss

        // compute t to find out where
        // the intersection point is
        var t = f * dotProduct(q, m_pE2);
        if(t > EPSILON) // ray intersection
            return [1, t];

        return false;

        // there is a line intersection
        // but not a ray intersection
        return false;
    }

    PlaneIntersectRay = function(plane, a_Ray, a_Dist)
    {
        var d = dotProduct(plane.normal, a_Ray.direction);
        if(d != 0) {
            var dist = -(dotProduct(plane.normal, a_Ray.origin) + plane.dimension) / d;
            if(dist > 0) {
                if(dist < a_Dist) {
                    return [1, dist];
                }
            }
        }
        return [0, a_Dist];
    }

    SphereIntersectRay = function(sphere, a_Ray, a_Dist)
    {
        var dst = {x: a_Ray.origin.x - sphere.center.x,
                   y: a_Ray.origin.y - sphere.center.y,
                   z: a_Ray.origin.z - sphere.center.z};

        var rDir = a_Ray.direction;
        var a = dotProduct(rDir, rDir);
        var b = 2*dotProduct(rDir, dst);
        var c = dotProduct(dst, dst) - (sphere.radius * sphere.radius);
        var disc = b*b - 4 * a * c;

        // if discriminant is negative there are no real roots, so return
        // false as ray misses sphere
        if(disc < 0)
            return false;

        var distSqrt = Math.sqrt(disc);
        if(b < 0)
            q = (-b - distSqrt)/2.0;
        else
            q = (-b + distSqrt)/2.0;

        var t0 = q / a;
        var t1 = c / q;

        var retval=0;
        if(t0 > t1) {
            var temp = t1;
            t1 = t0;
            t0 = temp;
            //    retval = -1; // ?? see when refraction enters
        }
        // if t1 is less than zero, the object is in the ray's negative direction
        // and consequently the ray misses the sphere
        if (t1 < 0)
            return false;

        // if t0 is less than zero, the intersection point is at t1
        if (t0 < 0) {
            if(t1 < a_Dist) {
                a_Dist = t1;
                retval = 1;
            }
        }
        // else the intersection point is at t0
        else {
            if(t0 < a_Dist) {
                a_Dist = t0;
                retval = 1;
            }
        }
        return [retval, a_Dist];
    };

    return {workerInit: workerInit,
            drawScan: drawScan,
            render: render,
            serverRun: serverRun,
            serverInit: serverInit};
}
