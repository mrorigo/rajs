/**
 * Worker script for RaJS Raytracer
 */
importScripts("raytracer.js");

var workerEngine = false;

onmessage = function(evt) {
  var data = evt.data;
  switch(data.type) {
  case 'id':
    if(!workerEngine)
      workerEngine = new Engine();
    workerEngine.workerInit(data.id, data.context);
    break;

  case 'render':  // render a scanline and post it back
    var scanline = true;
    while(1) {
      var scanline = workerEngine.render(workerEngine.workerContext);
      if(scanline !== false) {
	postMessage({type: 'scanline',
	      worker_id: workerEngine.workerId,
	      y: workerEngine.workerContext.m_CurrRow++,
	      data: scanline});
      } else {
	postMessage({type: 'finished',
	      y: workerEngine.workerContext.m_CurrRow-1,
	      worker_id: workerEngine.workerId});
	break;
      }
    }
    break;

  default:
    postMessage({type: 'error',
	         worker_id: workerEngine.workerId,
	         message: "Worker: " + workerEngine.workerId + " got an unknown message: " + evt.data});
    break;
  }
}
