{"materials":[{"color":{"r":0.8,"g":0.2,"b":0.2},"diffuse":0.7,"specular":0.8,"reflection":1},
              {"color":{"r":0.8,"g":0.8,"b":0.2},"diffuse":0.7,"specular":0.8,"reflection":1},
              {"color":{"r":0.8,"g":0.2,"b":0.8},"diffuse":0.7,"specular":0.8,"reflection":1},
              {"color":{"r":0.2,"g":0.4,"b":0.2},"diffuse":1,"specular":0.5,"reflection":0},
              {"color":{"r":0.3,"g":0.4,"b":0.3},"diffuse":1,"specular":0.5,"reflection":1}
             ],
 "primitives":[{"type":1,"center":{"x":0,"y":15,"z":-20},"intensity":0.1,"color":{"r":0.5,"g":0.5,"b":0.5}},
               {"type":3,"center":{"x":-3,"y":1.5,"z":-3},"radius":2.4,"material":0},
               {"type":3,"center":{"x":3.5,"y":1,"z":2},"radius":2,"material":1},
               {"type":3,"center":{"x":0,"y":-2,"z":-3},"radius":2,"material":2},
               {"type":2,"dimension":5,"normal":{"x":0,"y":1,"z":-0.1},"material":3},
               {"type":2,"dimension":16,"normal":{"x":-0.1,"y":-0.1,"z":-1},"material":4}
              ],
 "viewPlane":{"x1":-5,"x2":5,"y1":3.75,"y2":-3.75},
 "eyePosition":{"x":0,"y":0,"z":-15}
}

