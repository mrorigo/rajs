A "simple", or "naive" javascript raytracer using HTML5 Workers to perform the actual calculation of individual scanlines, and then join them together in the main thread and draw them to a HMTL5 Canvas to create the complete image.
 
Lots of inspiration and information came from [Jacco Bikker's article on DevMaster](http://www.devmaster.net/articles/raytracing_series/part1.php)
but I've lost track of what code came from where from the beginning..
The [main source](raytracer.js) (plus the [worker script](rt-worker.js)) is about 500 lines and is able to render any number of "Point" type lights and three different kind of primitives: Spheres, Triangles and Planes, which basically means you can render 'anything'. And you can, if you want, create your own scene for this raytracer, just (very carefully :) edit the scene definitions.

I've left shadows out for now, it just made everything too slow for my 1.33Mhz Atom CPU ;)

Rendering time really is improved by using more workers, up until a point. A single worker took 51 seconds to render Scene 2 on my wifes computer. While taking only 19 seconds with 2 workers, and 15 at 4 workers, more workers than this does not seem to improve on that system.

It all seems to work best in Chrome. Firefox does OK I guess, but Opera seems to crash alot. Oh, please feel free to use the code, it's "use and abuse" licensed, but let me know if you do, ok?
